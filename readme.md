# node-normalize-scss

This is simply a renamed [normalize.css 8.0.1](https://github.com/necolas/normalize.css) file, suitable for importing with npm and libsass directly. No changes have been made to the actual file.

### Install

```
npm install node-normalize-scss --save-dev
```

OR

```
yarn add node-normalize-scss --dev
```

## Using with [gulp-sass](https://github.com/dlmanning/gulp-sass)

```javascript
var gulp = require("gulp");
var sass = require("gulp-sass");

gulp.task("sass", function() {
  gulp
    .src("path/to/app.scss")
    .pipe(
      sass({
        includePaths: require("node-normalize-scss").includePaths
      })
    )
    .pipe(gulp.dest("path/to/output.css"));
});
```

## Using with [webpack](https://webpack.js.org/loaders/sass-loader/)

```javascript
module.exports = {
  module: {
    rules: [
      {
        use: [
          {
            loader: "sass-loader",
            options: {
              includePaths: require("node-normalize-scss").includePaths
            }
          }
        ]
      }
    ]
  }
};
```

## Using in scss

After creating your gulp task, you can import any of the resets like so:



```
@import 'normalize';
```
