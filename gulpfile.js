const gulp = require('gulp');
const rename = require('gulp-rename');
const done = function () { };


gulp.task('default', function (done) {
    gulp
        .src('./node_modules/normalize.css/normalize.css')
        .pipe(rename('_normalize.scss'))
        .pipe(gulp.dest('./node-normalize-scss/'));
    done();
});
